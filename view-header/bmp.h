#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "image.h"

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

/** описание заголовка
 */
struct __attribute__((packed)) bmp_header 
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};


/** смещение на мусорные байты в слчуае их наличия
  * img - изображение
 */
uint8_t get_padding(struct image img);

/** перечисление ошибок чтения
 */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NO_MEMORY,
    READ_ERROR,
    CANNOT_OPEN_FILE_FOR_READ
};

/** сопоставление ошибкам чтения их текстовых представлений
 */
static const char* read_status_string[] = {
    [READ_OK] = "File read successfully \n",
    [READ_INVALID_SIGNATURE] = "Invalid signature \n",
    [READ_INVALID_BITS] = "Invalid BitCount \n",
    [READ_INVALID_HEADER] = "Invalid header \n",
    [READ_NO_MEMORY] = "No memory \n",
    [READ_ERROR] = "Read error \n",
    [CANNOT_OPEN_FILE_FOR_READ] = "Cannot open file for read\n"
};


/** запись информации о картинке из bmp формата в структуру
  * in - поток ввода
  * img - изображение
 */
enum read_status from_bmp( FILE* in, struct image* img );

/** чтение информации из bmp файла
 * filename - имя файла
 * header - заголовок
 * img - изображение
 */
enum read_status read_from_bmp( const char* filename, struct bmp_header* header, struct image* img );

/** переисление ошибок записи
 */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    CANNOT_OPEN_FILE_FOR_WRITE
};

/** сопоставление ошибкам записи их текстовых представлений
 */
static const char* write_status_string[] = {
    [WRITE_OK] = "File recorded \n",
    [WRITE_ERROR] = "Write error \n",
    [CANNOT_OPEN_FILE_FOR_WRITE] = "Cannot open file for write \n"
};

/** запись информации из структуры изображения в bmp формат
  * out - поток вывода
  * img - изображение
 */
enum write_status to_bmp( FILE* out, const struct image* img );

/** запись в bmp файл
  * filename - имя файла
  * header - заголовок
  * img - изображение
 */
enum write_status write_to_bmp( const char* filename, const struct bmp_header* header, const struct image* img );

#endif
