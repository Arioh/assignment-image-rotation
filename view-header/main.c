#include <stdbool.h>
#include <stdio.h>
#include <memory.h>

#include "bmp.h"
#include "util.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) error("Not enough arguments \n" );
    if (argc > 2) error("Too many arguments \n" );

    struct bmp_header h = { 0 };
    struct image image = { 0 };

    enum read_status r =read_from_bmp( argv[1], &h, &image );
    if (r == READ_OK) {
        
        info(read_status_string[r]);
        struct image rimage = rotate(image);

        h.biHeight = (uint32_t)rimage.height;
        h.biWidth = (uint32_t)rimage.width;

        enum write_status w = write_to_bmp(argv[1], &h, &rimage);
        if(w != WRITE_OK) {
            error(write_status_string[w]);
        }
        else{
            info(write_status_string[w]);
        }
        destroy_image(rimage);
    }
    else {
        error(read_status_string[r]);
    }

    destroy_image(image);
    return 0;
}
