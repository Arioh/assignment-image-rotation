//
//  image.h
//  assignment-image-rotation
//
//  Created by Дима on 11/01/2021.
//  Copyright © 2021 Дима. All rights reserved.
//

#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <inttypes.h>
#include <stdio.h>

/** описание пикселя
 */
struct pixel { uint8_t b, g, r; };

/** описание изображения
 */
struct image {
    uint64_t width, height;
    struct pixel* data;
};
/** выделение памяти под изображение
  * w - ширина изображения в пикселях
  * h - высота изображения в пикселях
 */
struct image create_image(uint64_t w, uint64_t h);

/** освобождение памяти выделеннной под изображение
  * img - изображение
 */
void destroy_image(const struct image img);

/** создаёт копию изображения, которая повёрнута на 90 градусов
 */
struct image rotate( struct image const source );


#endif /* _IMAGE_H_ */
