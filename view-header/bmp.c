#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <memory.h>
#include <unistd.h>

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");



static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}


uint8_t get_padding(struct image img) {
    return (img.width * sizeof(struct pixel)) % 4 == 0 ? 0 : 4 - (img.width * sizeof(struct pixel)) % 4;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    uint8_t padding = get_padding(*img);
    for(size_t line = 0; line < img->height; ++line) {
        size_t pixels = fread(img->data + line * img->width, sizeof(struct pixel), img->width, in);
        if(pixels != img->width) {
            fclose(in);
            return READ_ERROR;
        }
        if(fseek(in, padding, SEEK_CUR) != 0) {
            fclose(in);
            return READ_ERROR;
        }
    }
    
    return READ_OK;
}

enum read_status read_from_bmp( const char* filename, struct bmp_header* header, struct image* img ) {
    if (!filename) return CANNOT_OPEN_FILE_FOR_READ;
    FILE* f = fopen( filename, "rb" );
    if (!f) return CANNOT_OPEN_FILE_FOR_READ;
    
    if(!read_header(f, header)) {
        fclose(f);
        return READ_INVALID_HEADER;
    }
    if(header->bfType != 0x4D42) {
        fclose(f);
        return READ_INVALID_SIGNATURE;
    }
    if(header->biBitCount != 24) {
        fclose(f);
        return READ_INVALID_BITS;
    }
    if(fseek(f, header->bOffBits, SEEK_SET) != 0) {
        fclose(f);
        return READ_ERROR;
    }
    
    *img = create_image(header->biWidth, header->biHeight);
    if(img->data == 0) {
        fclose(f);
        return READ_NO_MEMORY;
    }
    
    enum read_status result = from_bmp( f, img );

    fclose( f );
    return result;
}

static bool write_header( FILE* f, const struct bmp_header* header ) {
    if(fwrite( header, sizeof( struct bmp_header ), 1, f ) > 0) {
        fflush(f);
        return true;
    }
    return false;
}


enum write_status to_bmp( FILE* out, const struct image* img ) {
    uint8_t padding = get_padding(*img);
    for(size_t line = 0; line < img->height; ++line) {
        size_t pixels = fwrite(img->data + line * img->width, sizeof(struct pixel), img->width, out);
        if(pixels != img->width) {
            fclose(out);
            return WRITE_ERROR;
        }
        if(fseek(out, padding, SEEK_CUR) != 0) {
            fclose(out);
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}


enum write_status write_to_bmp( const char* filename, const struct bmp_header* header, const struct image* img ) {
    if (!filename) return CANNOT_OPEN_FILE_FOR_WRITE;
    FILE* f = fopen( filename, "wb" );
    if (!f) return CANNOT_OPEN_FILE_FOR_WRITE;
    
    if(write_header(f, header)) {
        if(fseek(f, header->bOffBits, SEEK_SET) == 0) {
            if (to_bmp( f, img ) == WRITE_OK) {
                fclose( f );
                return WRITE_OK;
            }
        }
    }
    
    fclose( f );
    return WRITE_ERROR;

}
