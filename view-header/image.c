//
//  image.c
//  assignment-image-rotation
//
//  Created by Дима on 11/01/2021.
//  Copyright © 2021 Дима. All rights reserved.
//

#include "image.h"
#include <stdlib.h>
#include <memory.h>
#include "util.h"
#include <inttypes.h>

struct image create_image(uint64_t width, uint64_t height) {
    struct image image = { 0 };
    
    image.width = width;
    image.height = height;
    image.data = malloc(width * height * sizeof(struct pixel));
    memset(image.data, 0, width * height * sizeof(struct pixel));

    return image;
}

void destroy_image(const struct image img) {
    free(img.data);
}

struct pixel get_pixel(uint64_t i, uint64_t j, const struct image img) {
    return *(img.data + i * img.width + j);
}

void set_pixel(struct pixel p, uint64_t i, uint64_t j, const struct image img) {
    *(img.data + i * img.width + j) = p;
}

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source ) {
    struct image dest = create_image(source.height, source.width);
    
    for(size_t i = 0; i < source.height; ++i) {
        for(size_t j = 0; j < source.width; ++j) {
            struct pixel p = get_pixel(i - 1, j, source);
            set_pixel(p, j, source.height - i - 1, dest);
        }
    }
    info("Image rotated \n");
    return dest;
}
