#ifndef _UTIL_H_
#define _UTIL_H_

/** вывод сообщения об ошибке
  * msg - текст сообщения
 */
_Noreturn void error( const char* msg, ... );

/** вывод информации о выполненных действиях
 * msg - текст сообщения
 */
void info( const char* msg, ... );
#endif
